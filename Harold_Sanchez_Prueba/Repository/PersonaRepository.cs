﻿using Harold_Sanchez_Prueba.Model;
using Harold_Sanchez_Prueba.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harold_Sanchez_Prueba.Repository
{
    public class PersonaRepository : dbConexion, IPersonaRepository
    {
        public OperationResult Create(Persona persona)
        {
            try
            {
                using (this.comando = new SqlCommand())
                {
                    this.comando.Connection = this.con;
                    comando.CommandText =
                        "INSERT INTO [dbo].[Persona]  ([Nombre],[FechaDeNacimiento],[Borrado])" +
                        $"VALUES (@Nombre,@FechaDeNacimiento,{0})";

                    comando.Parameters.AddWithValue("@Nombre", persona.Nombre);
                    comando.Parameters.AddWithValue("@FechaDeNacimiento", persona.FechaDeNacimiento);
                    this.con.Open();
                    try
                    {
                        comando.ExecuteNonQuery();
                        return new OperationResult() { Result = true };
                    }
                    catch (Exception ex)
                    {
                        return new OperationResult()
                        {
                            Result = false,
                            Message = $"Ha ocurrido un error. { ex.Message }"
                        };
                    }
                }

            }
            catch (SqlException ex)
            {
                return new OperationResult()
                {
                    Result = false,
                    Message = $"Ha ocurrido un error. { ex.Message }"
                };
            }
        }

        public DataTable GetAll()
        {
            this.con.Close();

            this.dt = new DataTable();
            using (this.comando = new SqlCommand())
            {
                this.comando.Connection = this.con;
                comando.CommandText = $"SELECT * FROM Persona where Borrado = 0";
                this.adapter = new SqlDataAdapter(comando);
                this.con.Open();
                adapter.Fill(dt);
                return dt;
            }
        }

        public DataTable GetById(int id)
        {
            this.con.Close();
            this.dt = new DataTable();
            using (this.comando = new SqlCommand())
            {
                this.comando.Connection = this.con;
                comando.CommandText = $"SELECT * FROM Persona where ID =@ID";
                comando.Parameters.AddWithValue("@ID", id);

                this.adapter = new SqlDataAdapter(comando);
                this.con.Open();
                adapter.Fill(dt);
                return dt;
            }
        }

        public OperationResult Update(int id, string nombre, DateTime fechaDeNacimiento)
        {
            try
            {
                this.con.Close();
                using (this.comando = new SqlCommand())
                {
                    this.comando.Connection = this.con;
                    comando.CommandText =
                        $"UPDATE [dbo].[Persona] SET [Nombre]=@Nombre,[FechaDeNacimiento]=@FechaDeNacimiento" +
                        $" WHERE ID =@ID";

                    comando.Parameters.AddWithValue("@Nombre", nombre);
                    comando.Parameters.AddWithValue("@FechaDeNacimiento", fechaDeNacimiento);
                    comando.Parameters.AddWithValue("@ID", id);

                    this.con.Open();
                    try
                    {
                        comando.ExecuteNonQuery();
                        return new OperationResult() { Result = true };
                    }
                    catch (Exception ex)
                    {
                        return new OperationResult()
                        {
                            Result = false,
                            Message = $"Ha ocurrido un error. { ex.Message }"
                        };
                    }
                }

            }
            catch (SqlException ex)
            {
                return new OperationResult()
                {
                    Result = false,
                    Message = $"Ha ocurrido un error. { ex.Message }"
                };
            }
        }
        public Persona _UpdatePersonar(Persona persona)
        {
            Read(See(GetAll()));

            int id = 0;
            Console.Write("\n\nIngresar ID .\nID:");
            if (int.TryParse(Console.ReadKey().KeyChar.ToString(), out id) && id > 0)
            {
                var _id = GetById(id);
                if (_id.DefaultView.Count > 0)
                {
                    var _see = See(_id);
                    foreach (var item in _see)
                    {
                        persona.ID = item.ID;
                        persona.Nombre = item.Nombre;
                        persona.FechaDeNacimiento = item.FechaDeNacimiento;
                        persona.Borrado = item.Borrado;
                    }

                    return new Persona
                    {
                        ID = persona.ID,
                        Nombre = persona.Nombre,
                        FechaDeNacimiento = persona.FechaDeNacimiento,
                        Borrado = persona.Borrado
                    };
                }
                else
                {
                    return null;
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("ID incorrecto, presione ENTER para intentelo de nuevo.!");
                Console.ReadLine();
                // _UpdatePersonar(persona);
                return _UpdatePersonar(persona);
            }

        }
        public OperationResult SoftDelete(int id, byte borrado)
        {
            try
            {

                this.con.Close();
                using (this.comando = new SqlCommand())
                {
                    this.comando.Connection = this.con;
                    comando.CommandText =
                        $"UPDATE [dbo].[Persona] SET Borrado=@Borrado" +
                        $" WHERE ID =@ID";

                    comando.Parameters.AddWithValue("@Borrado", borrado);
                    comando.Parameters.AddWithValue("@ID", id);

                    this.con.Open();
                    try
                    {
                        comando.ExecuteNonQuery();
                        return new OperationResult() { Result = true };
                    }
                    catch (Exception ex)
                    {
                        return new OperationResult()
                        {
                            Result = false,
                            Message = $"Ha ocurrido un error. { ex.Message }"
                        };
                    }
                }

            }
            catch (SqlException ex)
            {
                return new OperationResult()
                {
                    Result = false,
                    Message = $"Ha ocurrido un error. { ex.Message }"
                };
            }
        }

        public void Read(List<Persona> personas)
        {
            Console.Clear();
            if (personas.Count > 0)//(table.Rows.Count > 0)
            {
                Console.WriteLine("Lista de Personas\n");
                string S = String.Format("|{0,5}|{1,10}|{2,25}|", "ID", "Nombre", "Fecha de Nacimiento");
                Console.Write(S + "\n");
                foreach (var item in personas)
                {
                    S = String.Format("|{0,5}|{1,10}|{2,25}|", item.ID, item.Nombre, item.FechaDeNacimiento);
                    Console.Write(S + "\n");
                }
            }
            else
            {
                Console.WriteLine("No existen registros para mostrar." + "\nPresione enter para volver al menu.");
            }
        }

        public List<Persona> See(DataTable obj)
        {
            byte value;
            PersonaRepository sRepo = new PersonaRepository();
            List<Persona> personas = new List<Persona>();
            foreach (DataRow item in obj.Rows)
            {
                if (item[3].ToString() == "false")
                {
                    value = 0;
                }
                else
                {
                    value = 1;
                }
                personas.Add(new Persona
                {
                    ID = Convert.ToInt32(item[0].ToString()),
                    Nombre = (item[1].ToString()),
                    FechaDeNacimiento = Convert.ToDateTime((item[2])),
                    Borrado = value
                });
            }
            return personas;
        }
    }
}