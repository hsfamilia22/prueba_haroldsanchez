﻿using Harold_Sanchez_Prueba.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harold_Sanchez_Prueba.Repository
{
    public interface IPersonaRepository
    {
        DataTable GetAll();
        DataTable GetById(int id);
        OperationResult Create(Persona persona);
        OperationResult Update(int id, string Nombre, DateTime fechaNacimiento);

        OperationResult SoftDelete(int id, byte borrar);

    }
}
