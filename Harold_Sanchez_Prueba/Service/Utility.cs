﻿using Harold_Sanchez_Prueba.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harold_Sanchez_Prueba.Service
{
    public class Utility
    {

        public Persona CreatePersona(Persona persona)
        {
            Console.Write("Nombre: ");
            persona.Nombre = Console.ReadLine();
            try
            {
                Console.Write("Fecha de Nacimiento(dd-MM-yyyy): ");
                persona.FechaDeNacimiento = Convert.ToDateTime(Console.ReadLine());
            }
            catch (Exception)
            {

                Console.Write("La Fecha de Nacimiento es invalida(dd-MM-yyyy): ");
            }
            

            persona.Borrado = 0;
            Console.Clear();
            return persona;
        }

        public Persona ActualizarPersona(int id, string nombre, DateTime fechaNacimiento)
        {
            Console.Write("\nEsta seguro que desea actualizar este registro?\nPresione 'S' para continuar de lo contrario pulse cualquier tecla: ");
            string value = Console.ReadLine();
            if (value.ToLower() == "s")
            {
                Console.Clear();
                Console.WriteLine("Ingrese los datos a Actualizar\n");
                Console.Write("Nombre: ");
                nombre = Console.ReadLine();
                while (true)
                {
                    try
                    {
                        Console.WriteLine("Ingrese los datos a Actualizar\n");
                        Console.Write("fechaNacimiento(MM-dd-yyyy): ");
                        fechaNacimiento = Convert.ToDateTime(Console.ReadLine());
                        break;
                    }
                    catch (Exception)
                    {
                        Console.Clear();
                        Console.WriteLine("Fecha Incorrecta Intentelo de nuevo");

                        continue;
                    }
                }

                Console.Clear();
                return new Persona
                {
                    ID = id,
                    Nombre = nombre,
                    FechaDeNacimiento = fechaNacimiento
                };
            }
            else
            {
                Console.Clear();
                return null;
            }
        }
        public Persona BorrarPersonar(int id)
        {
            Console.Write("\nEsta seguro que desea borrar este registro?\nPresione 'S' para continuar de lo contrario pulse cualquier tecla: ");
            string value = Console.ReadLine();
            if (value.ToLower() == "s")
            {
                Console.Clear();
                return new Persona
                {
                    ID = id,
                    Borrado = 1
                };
            }
            else
            {
                Console.Clear();
                return null;
            }
        }
    }
}
