﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harold_Sanchez_Prueba.Model;
using Harold_Sanchez_Prueba.Repository;
using Harold_Sanchez_Prueba.Service;

namespace Harold_Sanchez_Prueba
{
    class Program
    {
        string mensajeError = null;
        static void Main(string[] args)
        {
            
            bool valid = false;
            int option = 0;
            Persona _persona = new Persona();
            while (valid == false)
            {
                Console.Clear();
                Console.WriteLine("Aplicación tipo CRUD");
                Console.WriteLine("**************Menu*************\n\n1- Crear Persona\n2- Buscar Persona\n3- Buscar por ID\n4- Actualizar Persona\n5- Borrar Persona\n6- Salir");
                if (int.TryParse(Console.ReadKey().KeyChar.ToString(), out option) && option <= 6)
                {
                    if (option == 1)
                    {
                        Console.Clear();
                        Console.WriteLine("Crear Persona\n");
                        Console.WriteLine(Agregar(_persona).Message + "\n\nPresione enter para volver al menu.");
                        Console.ReadLine();
                    }
                    else if (option == 2)
                    {
                        Console.Clear();
                        Console.WriteLine("Lista de Personas\n");
                        BuscarALL();
                        Console.WriteLine("\n\nPresione enter para volver al menu.");
                        Console.ReadLine();
                    }
                    else if (option == 3)
                    {
                        Console.Clear();
                        Console.WriteLine("Busqueda de Personas por ID\n");
                        try
                        {
                            Console.Write("Ingrese ID: ");
                            int id = Convert.ToInt32(Console.ReadLine());

                            if (id > 0)
                            {
                                BuscarID(id);
                                Console.WriteLine("\n\nPresione enter para volver al menu.");
                                Console.ReadLine();
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.Clear();
                            Console.WriteLine("Sentencia Incorrecta, Vuelva a Intentarlo\n\nPresione enter para volver al menu.");
                            Console.ReadLine();
                        }
                    }
                    else if (option == 4)
                    {
                        Console.Clear();
                        Console.WriteLine("Actualizar Persona\n");
                        Console.WriteLine(Actualizar().Message + "\n\nPresione enter para volver al menu.");
                        Console.ReadLine();
                    }
                    else if (option == 5)
                    {
                        Console.Clear();
                        Console.WriteLine("Borrar Persona\n");
                        Console.WriteLine(Borrar(_persona).Message + "\n\nPresione enter para volver al menu.");
                        Console.ReadLine();
                    }
                    else
                    {
                        valid = true;
                    }
                }
            }
        }

        private static OperationResult Borrar(Persona persona)
        {
            PersonaRepository repo = new PersonaRepository();
            Utility util = new Utility();

            var obj = repo._UpdatePersonar(persona);
            if (obj != null)
            {
                obj = util.BorrarPersonar(obj.ID);
                if (repo.SoftDelete(obj.ID, obj.Borrado).Result == true)
                {
                    return new OperationResult
                    {
                        Result = true,
                        Message = "Registro Actualizado!!!"

                    };

                }
                else
                {
                    Console.Clear();
                    return new OperationResult
                    {
                        Result = false,
                        Message = "Error Actualizando Persona"
                    };
                }
            }
            else
            {
                return new OperationResult
                {
                    Result = false,
                    Message = "Error Actualizando Persona"
                };
            }
        }

        private static OperationResult Actualizar()
        {
            PersonaRepository repo = new PersonaRepository();
            Persona persona = new Persona();
            Utility util = new Utility();

            var obj = repo._UpdatePersonar(persona);
            if (obj != null)
            {
                obj = util.ActualizarPersona(obj.ID, obj.Nombre, obj.FechaDeNacimiento);
                if (obj != null)
                {
                    if (repo.Update(obj.ID, obj.Nombre, obj.FechaDeNacimiento).Result == true)
                    {
                        return new OperationResult
                        {
                            Result = true,
                            Message = "Registro Actualizado!!!"

                        };
                    }
                    else
                    {
                        Console.Clear();
                        return new OperationResult
                        {
                            Result = false,
                            Message = "Error Actualizando Persona"
                        };
                    }
                }
                else
                {
                    Console.Clear();
                    return new OperationResult
                    {
                        Result = false,
                        Message = "Error Actualizando Persona"
                    };
                }
            }
            else
            {
                return new OperationResult
                {
                    Result = false,
                    Message = "Error Actualizando Persona"
                };
            }
        }

        private static void BuscarID(int id)
        {
            PersonaRepository repo = new PersonaRepository();
            repo.Read(repo.See(repo.GetById(id)));
        }

        static void BuscarALL()
        {
            PersonaRepository repo = new PersonaRepository();
            repo.Read(repo.See(repo.GetAll()));
        }

        static OperationResult Agregar(Persona persona)
        {
            PersonaRepository repo = new PersonaRepository();
            Utility util = new Utility();
            util.CreatePersona(persona);
            if (persona != null)
            {
                if (repo.Create(persona).Result == true)
                {
                    return new OperationResult
                    {
                        Data = persona,
                        Result = true,
                        Message = "Registro insertado!!!"
                    };
                }
                else
                {
                    try
                    {
                        return new OperationResult
                        {
                            Result = false,
                            Message = "Error Agregando Persona"
                        };
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    
                }
            }
            else
            {
                return new OperationResult
                {
                    Result = false,
                    Message = "Error Agregando Persona"
                };
            }
        }
    }
}