﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harold_Sanchez_Prueba.Model
{
    public class Persona
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaDeNacimiento { get; set; }

        public byte Borrado { get; set; }
    }
}