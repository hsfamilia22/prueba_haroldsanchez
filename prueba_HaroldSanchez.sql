
/*crear base de datos correspondiente*/
create database HaroldSanchez_Prueba;
go


/*Poner en uso la base de datos creada anterior HaroldSanchez_Prueba*/
use HaroldSanchez_Prueba;
go


/*Crear tabla personas*/
CREATE TABLE Persona(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](30) NULL,
	[FechaDeNacimiento] [date] NULL,
	[Borrado] [bit] NOT NULL,);